function jsx(tag, attrs) {
    var children = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        children[_i - 2] = arguments[_i];
    }
    var element = document.createElement(tag);
    for (var name_1 in attrs) {
        if (name_1 && attrs.hasOwnProperty(name_1)) {
            var value = attrs[name_1];
            if (value === true) {
                element.setAttribute(name_1, name_1);
            }
            else if (value !== false && value != null) {
                if (typeof value == 'function' && name_1.indexOf("on") == 0)
                    element.addEventListener(name_1.substr(2), value);
                else
                    element.setAttribute(name_1, value.toString());
            }
        }
    }
    for (var _a = 0, children_1 = children; _a < children_1.length; _a++) {
        var child = children_1[_a];
        element.appendChild(child.nodeType == null
            ? document.createTextNode(child.toString())
            : child);
    }
    return element;
}
