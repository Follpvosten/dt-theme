var kingdoms = ["Blauküste", "Donnerhall", "Eisenwinde", ""];
var tests = [{
        name: "Zu welchem Königshaus gehörst du?",
        profiles: [
            { "name": "Blauküste", description: "" },
            { "name": "Donnerhall", description: "" },
            { "name": "Eisenwinde", description: "" }
        ],
        questions: [{
                text: "Magst du das Krümelmonster?",
                answers: [
                    { text: "Ja", profileBoosts: { 0: 0, 1: 0, 2: 1 } },
                    { text: "Nein", profileBoosts: { 1: 1 } },
                    { text: "Vielleicht", profileBoosts: { 2: 1 } }
                ]
            }]
    },
    {
        name: "Welches Gebiet ist deine Heimat?",
        profiles: [
            { "name": "Monarch", description: "" },
            { "name": "Leibwächter", description: "" },
            { "name": "Königlicher Berater", description: "" },
            { "name": "Priester", description: "" }
        ],
        questions: [{
                text: "Magst du das Krümelmonster?",
                answers: [
                    { text: "Ja", profileBoosts: { 0: -1, 2: 1 } },
                    { text: "Nein", profileBoosts: { 1: 1 } },
                    { text: "Vielleicht", profileBoosts: { 2: 1 } }
                ]
            }, {
                text: "Magst du Kermit?",
                answers: [
                    { text: "Ja", profileBoosts: { 0: 1, 1: 2, 3: -2 } },
                    { text: "Nein", profileBoosts: { 1: 1 } },
                    { text: "Deine Mudda", profileBoosts: { 2: 2 } }
                ]
            }]
    }];
