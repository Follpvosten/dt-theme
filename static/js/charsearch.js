var resultsList;
var searchInput;
var characters = [
    {
        "name": "207",
        "description": "Sensenmann"
    },
    {
        "name": "Aaron Baumann/Griffon",
        "description": "Monicas Geigenlehrer",
        "url": "../aaron"
    },
    {
        "name": "Affe A",
        "description": "Mitglied der Affenbande, stummer und bedrohlicher Gorilla"
    },
    {
        "name": "Affe B",
        "description": "Mitglied der Affenbande, Skater-Primat mit kräftigem Vorbiss"
    },
    {
        "name": "Annika Bartels",
        "description": "Tochter von Herrn Bartels mit einem sehr eigenartigen Charakter"
    },
    {
        "name": "Antonia Finkel",
        "description": "Große Schwester von Henrietta"
    },
    {
        "name": "Aurorafeder",
        "description": "Schlossbibliothekarin, die die Regale voller Bücher wie ihr eigenes Zuhause kennt"
    },
    {
        "name": "Avelyna",
        "description": "Monicas kleine Cousine besitzt mehr Gehirn als ihre großen Brüder"
    },
    {
        "name": "Baumhardt",
        "description": "Portalbaum von Grünewald"
    },
    {
        "name": "Ben Mihtena",
        "description": "Gefürchteter Anführer der Bestillen"
    },
    {
        "name": "Bernadette/Bernie",
        "description": "Zwillingsschwester von Fiete, lebt mit ihrem Vater in Berlin"
    },
    {
        "name": "Bertha",
        "description": "Älteste Zofe im Palast der Flügelklippe"
    },
    {
        "name": "Besitzer der Wüstenrennerfarm",
        "description": "Pflegt und versorgt die Wüstenrenner, sein Haus wird öfter Opfer von Hanamis und Tanyias Trainingsstunden"
    },
    {
        "name": "Billy Oxberg",
        "description": "Anführer der Affenbande"
    },
    {
        "name": "Blütenstaub",
        "description": "Königlicher Gärtner von Grünewald und Jazzys Ehemann"
    },
    {
        "name": "Brummir",
        "description": "Zweiter Offizier der königlichen Armee von Grünewald"
    },
    {
        "name": "Chantal",
        "description": "Monicas Klassenkameradin, interessiert sich für ägyptische Götter „oder so“"
    },
    {
        "name": "Daan Vleugel",
        "description": "Adoptivvater von Monica"
    },
    {
        "name": "Darcy Dalskär/Seidenschuppe",
        "description": "Ritterin der Armee von Flügelkippe, große Schwester von Timo"
    },
    {
        "name": "Das Biest mit den roten Augen",
        "description": "Der Ursprung"
    },
    {
        "name": "Derek Whittaker",
        "description": "Polizist mit einer Schwäche für Donuts und Bären"
    },
    {
        "name": "Diana Monteno/Aquamarinfluss",
        "description": "Königin von Donnerhall"
    },
    {
        "name": "Dirk Autentahl/Stachelstein",
        "description": "Leibwächter der Montenos und gute Seele, verwandelt sich gerne in ein Stoppschild"
    },
    {
        "name": "Donnerzahn",
        "description": "Königlicher Berater der Flügelklippe, besitzt ein Auge für Ästhetik und ist pünktlicher als jede Uhr"
    },
    {
        "name": "Doris Stark",
        "description": "Monicas verstorbene Oma mütterlicherseits"
    },
    {
        "name": "Eduardo",
        "description": "Hauptmann der königlichen Armee von Grünewald"
    },
    {
        "name": "Elea",
        "description": "Königliche Beraterin von Grünewald"
    },
    {
        "name": "Elfenflug",
        "description": "Mutter von Aaron"
    },
    {
        "name": "Epione Bellatoxic",
        "description": "Damalige, verstorbene Prinzessin von Grünewald"
    },
    {
        "name": "Esther Amber Holly-Clover Ginger Luna Moon da Lilly del Sol",
        "description": "Monicas verschwundene Tante"
    },
    {
        "name": "Fate Et‘haka",
        "description": "Cousine von Monica und Tochter von Miracle und Rune"
    },
    {
        "name": "Ferdinand Faust",
        "description": "Zweite Offizierin der königlichen Armee der Flügelklippe"
    },
    {
        "name": "Feuerspitze",
        "description": "Unter mysteriösen Umständen verschwundener Drache, der durch ebenso unheimliche Umstände gestorben ist"
    },
    {
        "name": "Fiete Bierbach",
        "description": "Monicas Freund, Vorzeigestreber und Roboter-Fanatiker"
    },
    {
        "name": "Frank",
        "description": "Einhorn-Partner von Eduardo"
    },
    {
        "name": "Frau (Ruth) Grimm",
        "description": "Bibliothekarin und Esoterikerin aus Monicas Schule"
    },
    {
        "name": "Frau (Birte) Gurkenheimer",
        "description": "Nette, ältere Dame im Dorf, lebende Zeitung und schwärmt (nur heimlich) für Aaron"
    },
    {
        "name": "Frau (Gretel) Müllermeier",
        "description": "Unterrichtet Biologie und liebt fesselnde Erotik-Romane"
    },
    {
        "name": "Frau (Debbie) Reinhard",
        "description": "Direktorin der Grundschule, hofft auf ein Treffen mit den Außerirdischen"
    },
    {
        "name": "Frau vom Nerd unter der Treppe",
        "description": "Arbeitet fleißig an ihrem Roman und versorgt ihren Mann mit Nahrung"
    },
    {
        "name": "Frida",
        "description": "Hauptmann der königlichen Armee von Donnerhall"
    },
    {
        "name": "Gaston",
        "description": "Liselottes Yorkshire Terrier und guter Junge"
    },
    {
        "name": "Graufeder",
        "description": "Aarons fette, überfütterte Haustaube"
    },
    {
        "name": "Großonkel Luke",
        "description": "Wer ist Großonkel Luke?"
    },
    {
        "name": "Grünbart",
        "description": "Vater von Aaron"
    },
    {
        "name": "Guinevere/Silberflügel",
        "description": "Heiligodems Tochter und Priesterin"
    },
    {
        "name": "Gulasch",
        "description": "Hyperaktive Schildpatt-Katze mit Kuschelfunktion"
    },
    {
        "name": "Gustave Bourgeoise",
        "description": "Begnadeter Schwertkämpfer aus Frankreich und Nesthäkchen der königlichen Armee der Flügelklippe"
    },
    {
        "name": "Hanami",
        "description": "A-Klasse Kampfmagierin und B-Klasse Heilerin aus Taros Dorf"
    },
    {
        "name": "Heiligodem",
        "description": "Ein kauziger, alter Priester mit schwachem Gedächtnis"
    },
    {
        "name": "Henrietta Tina Finkel",
        "description": "Monicas beste Freundin und eine waschechte Piratenbraut"
    },
    {
        "name": "Herr (Erik) Bartels",
        "description": "Mathelehrer und Bilgratte"
    },
    {
        "name": "Hotaru/Taro",
        "description": "Bester Freund von Monica und voll cooler Ninja in Ausbildung",
        "url": "../taro"
    },
    {
        "name": "Houston Nephirit",
        "description": "König von Redemia"
    },
    {
        "name": "Idris Kaheel",
        "description": "Großer Bruder von Zuleika"
    },
    {
        "name": "Jacqueline",
        "description": "Monicas Klassenkameradin, vertraut auf Daddys Kreditkarte"
    },
    {
        "name": "Janine Vleugel",
        "description": "Adoptivmutter von Monica"
    },
    {
        "name": "Jasper",
        "description": "Angestellter im Palast der Flügelklippe, der sich wie ein Hofnarr kleidet"
    },
    {
        "name": "Jazzy",
        "description": "Königliche Apothekerin von Grünewald und Blütenstaubs Ehefrau"
    },
    {
        "name": "Jeremy",
        "description": "Königlicher Schneider mit exquisitem Geschmack"
    },
    {
        "name": "Jürgen und Günther Gurkenheimer",
        "description": "Große Brüder von Frau Gurkenheimer und in der Filmbranche tätig"
    },
    {
        "name": "Kasimir Käseblatt",
        "description": "Journalist, Reporter, Chefredakteur, Senderchef – erfährt jedes noch so kleine Geheimnis"
    },
    {
        "name": "Kuzahse",
        "description": "Rivale von Taro"
    },
    {
        "name": "Kyle Yanman/Nachtschwinge",
        "description": "Ritter der königlichen Armee von Flügelklippe, hängt ständig mit Ragnar ab und ist der Extrovertierte"
    },
    {
        "name": "Lavasturm",
        "description": "Gott der Drachen und der erste seiner Art"
    },
    {
        "name": "Leyla",
        "description": "Tatkräftige Puitzkraft im Palast der Flügelklippe"
    },
    {
        "name": "Linda Stil",
        "description": "Unhöfliche Dame, die Vorurteile gegen Jugendliche hat"
    },
    {
        "name": "Liselotte",
        "description": "Nachbarin der Vleugels und beschäftigte Geschäftsfrau"
    },
    {
        "name": "Lucia Monteno/Blauschuppe",
        "description": "Prinzessin von Donnerhall mit einer Vorliebe für Kampfsport und besondere Spitznamen",
        "url": "../lucia"
    },
    {
        "name": "Ludger Monteno",
        "description": "König von Donnerhall"
    },
    {
        "name": "Lukas und Bill",
        "description": "Monicas Cousins und potentielle Anwärter auf die Affenbande"
    },
    {
        "name": "Maggie Bonnie Samira-Elodie",
        "description": "Monicas Tante, die sich wenig für ihre Nichte interessiert"
    },
    {
        "name": "Magnus Tyrilli/Sichelmond",
        "description": "Genervter Teenager mit einer Vorliebe für Echsen"
    },
    {
        "name": "Margarette",
        "description": "Schüchterne Zofe mit der Ausstrahlung einer kleinen Schwester"
    },
    {
        "name": "Matope, der Kunzit",
        "description": "Vierter Edelstein, war Partner von König Euphobius"
    },
    {
        "name": "Mave",
        "description": "Köchin im Palast der Flügelklippe, schmuggelt Essen für Rose und erntet dafür ihre endlose Liebe"
    },
    {
        "name": "Maxwellthorthonsen/ Diamantodem",
        "description": "Wird auch als Max abgekürzt, Tiroler-Bua mit hitzigem Gemüt"
    },
    {
        "name": "McFrog, der Amethyst",
        "description": "Zweiter Edelstein, hat nun auf ewig Angst vor Janine und ihrer Bratpfanne, war Partner von König Paulus"
    },
    {
        "name": "Miracle Elena-Destiny Wonder Et‘haka",
        "description": "Monicas Tante und Hexe"
    },
    {
        "name": "Monica Vleugel/Luna Moon da Lilly del Sol",
        "description": "Unsere Hauptcharakterin",
        "url": "../monica"
    },
    {
        "name": "Murat Kaheel",
        "description": "Kleiner Bruder von Zuleika, der gerne Flugzeuge imitiert und seine Geschwister nervt"
    },
    {
        "name": "Nerd unter der Treppe",
        "description": "Eigentlich IT-Technikexperte und im Pui-Fanclub"
    },
    {
        "name": "Nikolassedoruben",
        "description": "Herrscher des Waldes"
    },
    {
        "name": "Oma Anissa",
        "description": "Monicas Großmutter väterlicherseits, lebt in den Niederlanden"
    },
    {
        "name": "Oma Kunigunde",
        "description": "Monicas angeheiratete Großmutter mütterlicherseits, strickt gerne Katzenkleidung"
    },
    {
        "name": "Opa Kurt",
        "description": "Monicas Großvater mütterlicherseits mit gutem Gehör"
    },
    {
        "name": "Oslo Skarvswind/Orkanpfeil",
        "description": "Hauptmann der königlichen Armee von Flügelklippe und Vorzeigekavalier"
    },
    {
        "name": "Pàgocies, der Saphir",
        "description": "Erster Edelstein, war der Partner von Königin Nesteren"
    },
    {
        "name": "Peanut",
        "description": "Jazzys und Blütenstaubs verstorbener Sohn"
    },
    {
        "name": "Pilzkiefer",
        "description": "Chefkoch im Palast von Grünewald und Hobby-Botaniker"
    },
    {
        "name": "Puigi",
        "description": "Chefkoch der königlichen Küche von Flügelklippe"
    },
    {
        "name": "Purzel",
        "description": "Weißes Kaninchen, kleines, flauschiges Wutpaket"
    },
    {
        "name": "Puschel und Pummel",
        "description": "Braunes und graues Zwergkaninchen, die gerne kuscheln"
    },
    {
        "name": "Ragnar Stugvik",
        "description": "Ritter der königlichen Armee von Flügelklippe, hängt immer mit Kyle ab und ist der Introvertierte"
    },
    {
        "name": "Rosa",
        "description": "Riesiger, schweinchenrosa Oktopus, der die Gewässer Draconicas, auf der Suche nach Abenteuern, durchkämmt"
    },
    {
        "name": "Rose/Schemenklaue",
        "description": "Kecke Zofe mit viel Eyeliner und einem endlosen Hunger"
    },
    {
        "name": "Rostschnabel",
        "description": "Roter Greif, beste Freundin von Graufeder"
    },
    {
        "name": "Rune Et‘haka",
        "description": "Onkel von Monica, Mann von Miracle und Hexer, bevorzugt aber den Begriff Warlock"
    },
    {
        "name": "Sangeri, der Granat",
        "description": "Dritter Edelstein, war Partnerin von Königin Rasmine"
    },
    {
        "name": "Saturn",
        "description": "Brauner Hannoveraner von Fiete"
    },
    {
        "name": "Schnucki/Edgar",
        "description": "Frau Stils Mops, der keine Vorurteile gegen Jugendliche hat"
    },
    {
        "name": "Seppuku",
        "description": "Mysteriöser Roboter-Samurai"
    },
    {
        "name": "Seyna Destiny Day/Luna Moon da Lilly del Sol",
        "description": "Ehemalige Königin der Flügelklippe, Monicas Mutter und talentierte Sängerin"
    },
    {
        "name": "Spargel",
        "description": "Faule und fette Katze mit feuerrotem Fell, nützlicher als jeder Staubsauger"
    },
    {
        "name": "Steve und Rick",
        "description": "Zwei Einhörner aus Grünewald"
    },
    {
        "name": "Sunshine P./Percy Peperoni",
        "description": "Direktor von Monicas Schule"
    },
    {
        "name": "Susanne Kneiper",
        "description": "Polizistin mit Durchblick und gewissenhafter Einstellung, ihr Mann ist Tierarzt"
    },
    {
        "name": "Sven Monteno",
        "description": "Prinz und königlicher Berater von Donnerhall"
    },
    {
        "name": "Tante Franziska",
        "description": "Monicas Tante und staatlich anerkannter Hypochonder"
    },
    {
        "name": "Tanyia",
        "description": "Beste Freundin/Schülerin von Hanami aus Taros Dorf"
    },
    {
        "name": "Timo Dalskär/Hammerwucht",
        "description": "Kleiner Bruder von Darcy, arbeitet in der Palastküche von Flügelklippe"
    },
    {
        "name": "Tithus Alexander-Flavius Aurelius Reximus Luna Moon da Lilly del Sol XIV",
        "description": "Ehemaliger König der Flügelklippe, Monicas Vater und definitiv kein Zirkusaffe"
    },
    {
        "name": "Typ mit den auffällig behaarten Nasenlöchern",
        "description": "Beseitigt den Schaden, den andere in Taros Dorf anrichten"
    },
    {
        "name": "Wasserklinge",
        "description": "Wassertaxi"
    },
    {
        "name": "Wasserstern",
        "description": "Geschichtenerzählerin"
    },
    {
        "name": "Zuleika Kaheel",
        "description": "Monicas Freundin mit einer Schwäche für Mode und Superhelden"
    },
];
function render(char) {
    var charname = char.url ?
        jsx("a", { href: char.url }, char.name)
        : char.name;
    return jsx("div", { class: "column is-half" },
        jsx("div", { class: "notification" },
            jsx("h5", { class: "title has-text-black is-5" }, charname),
            jsx("div", { class: "content has-text-black" }, char.description)));
}
function search() {
    resultsList.innerHTML = "";
    var search = searchInput.value.toLowerCase();
    if (!search)
        return;
    var titleMatches = characters.filter(function (c) { return c.name.toLowerCase().indexOf(search) > -1; });
    var descrMatches = characters.filter(function (c) { return c.description.toLowerCase().indexOf(search) > -1
        && titleMatches.indexOf(c) == -1; });
    if (titleMatches.length) {
        resultsList.appendChild(jsx("div", { class: "column is-full" },
            jsx("h2", { class: "title is-4" }, "Ergebnisse in Namen")));
        for (var _i = 0, titleMatches_1 = titleMatches; _i < titleMatches_1.length; _i++) {
            var char = titleMatches_1[_i];
            resultsList.appendChild(render(char));
        }
    }
    if (descrMatches.length) {
        resultsList.appendChild(jsx("div", { class: "column is-full" },
            jsx("h2", { class: "title is-4" }, "Ergebnisse in Beschreibungen")));
        for (var _a = 0, descrMatches_1 = descrMatches; _a < descrMatches_1.length; _a++) {
            var char = descrMatches_1[_a];
            resultsList.appendChild(render(char));
        }
    }
}
document.addEventListener("DOMContentLoaded", function () {
    resultsList = document.getElementById("results_list");
    searchInput = document.getElementById("search");
    searchInput.addEventListener("keyup", function (e) {
        if (e.keyCode == 13)
            searchInput.blur();
        else
            search();
    });
    search();
});
