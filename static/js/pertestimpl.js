var Test;
(function (Test) {
    var storageSupport;
    var currentTest;
    var currentQuestion;
    var currentQuestionIndex;
    var currentAnswers;
    var testContent;
    var questionContainer;
    var answersContainer;
    var backButton;
    function init() {
        // Check for localStorage
        storageSupport = typeof Storage !== 'undefined';
        testContent = document.getElementById("testContent");
        if (storageSupport) {
            var lastGame = localStorage.getItem("currentTest");
            if (lastGame) {
                loadSavedTest(JSON.parse(lastGame));
            }
            else {
                renderMainMenu();
            }
        }
        else {
            renderMainMenu();
        }
    }
    function renderMainMenu() {
        testContent.innerHTML = "";
        var _loop_1 = function (test) {
            testContent.appendChild(jsx("div", { class: "column is-half", onclick: function () { return startTest(test); } },
                jsx("div", { class: "notification" },
                    jsx("h1", { class: "title has-text-black is-5" }, test.name))));
        };
        for (var _i = 0, tests_1 = tests; _i < tests_1.length; _i++) {
            var test = tests_1[_i];
            _loop_1(test);
        }
    }
    function renderTestView() {
        backButton = jsx("button", { class: "button", onclick: renderMainMenu }, "Zur\u00FCck");
        var buttonWrapper = jsx("div", { class: "column is-full" }, backButton);
        questionContainer = jsx("h1", { class: "title has-text-black is-5" });
        answersContainer = jsx("div", null);
        var newContent = jsx("div", { class: "columns is-multiline" },
            buttonWrapper,
            jsx("div", { class: "column is-half" },
                jsx("div", { class: "notification" }, questionContainer)),
            jsx("div", { class: "column is-half" },
                jsx("div", { class: "notification" },
                    answersContainer,
                    jsx("div", { class: "buttons is-right" },
                        jsx("button", { class: "button is-dark", onclick: chooseOption }, "Best\u00E4tigen")))));
        testContent.replaceWith(newContent);
        testContent = newContent;
    }
    function showQuestion(question) {
        questionContainer.innerText = question.text;
        answersContainer.innerHTML = question.answers
            .map(function (a, i) { return "<p class=\"control\"><label class=\"radio has-text-black\">\n  <input type=\"radio\" name=\"answer\" value=\"" + i + "\">\n  " + a.text + "\n</label></p>"; })
            .join("");
    }
    function startTest(test) {
        currentTest = test;
        currentAnswers = [];
        // Build test view
        renderTestView();
        currentQuestionIndex = 0;
        currentQuestion = currentTest.questions[currentQuestionIndex];
        showQuestion(currentQuestion);
    }
    function loadSavedTest(data) {
        var testsFound = tests.filter(function (t) { return t.name == data.testName; });
        if (!testsFound) {
            renderMainMenu();
            return;
        }
        var test = testsFound[0];
        // Test restoring logic
    }
    function nextQuestion() {
        currentQuestionIndex++;
        currentQuestion = currentTest.questions[currentQuestionIndex];
        showQuestion(currentQuestion);
    }
    function chooseOption() {
        console.log("Choose clicked");
        var selectedElem = $("input[name=answer]:checked").item(0);
        if (selectedElem == null) {
            // TODO: Tell the user to select something
            console.log("No answer selected");
        }
        else {
            // Implement logic for going further here
            console.log("Chosen answer: " + selectedElem.value);
            currentAnswers.push(Number(selectedElem.value));
            nextQuestion();
        }
    }
    document.addEventListener("DOMContentLoaded", init, false);
})(Test || (Test = {}));
