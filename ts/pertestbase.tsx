const $ = (selector: string) => document.querySelectorAll(selector);

interface NumberDict {
    [key: number]: number;
}

interface PersonalityTest {
    name: string;
    profiles: Profile[];
    questions: Question[];
}

interface Question {
    text: string;
    answers: { text: string; profileBoosts: NumberDict }[]
}

interface Profile {
    name: string;
    description: string;
}

interface GameData {
    testName: string;
    currentQuestion: number;
    answers: number[];
}

/* function jsx(tag: string, attrs: { [key: string]: any }, children: any[]) {
    var element = document.createElement(tag);

    for (let name in attrs) {
        if (name && attrs.hasOwnProperty(name)) {
            let value = attrs[name];
            if (value === true) {
                element.setAttribute(name, name);
            } else if (value !== false && value != null) {
                if (typeof value == 'function' && name.indexOf("on") == 0)
                    element.addEventListener(name.substr(2), value);
                else
                    element.setAttribute(name, value.toString());
            }
        }
    }
    for (let i = 2; i < arguments.length; i++) {
        let child = arguments[i];
        element.appendChild(
            child.nodeType == null ?
                document.createTextNode(child.toString()) : child);
    }
    return element;
}
 */