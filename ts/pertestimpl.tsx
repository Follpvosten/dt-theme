declare namespace JSX {
    interface IntrinsicElements {
        [elemName: string]: any;
    }
}

namespace Test {
    var storageSupport: boolean;

    var currentTest: PersonalityTest;
    var currentQuestion: Question;
    var currentQuestionIndex: number;
    var currentAnswers: number[];

    var testContent: HTMLDivElement;
    var questionContainer: HTMLHeadingElement;
    var answersContainer: HTMLDivElement;
    var backButton: HTMLButtonElement;

    function init() {
        // Check for localStorage
        storageSupport = typeof Storage !== 'undefined';
        testContent = document.getElementById("testContent") as HTMLDivElement;
        if (storageSupport) {
            let lastGame = localStorage.getItem("currentTest");
            if (lastGame) {
                loadSavedTest(JSON.parse(lastGame));
            } else {
                renderMainMenu();
            }
        } else {
            renderMainMenu();
        }
    }

    function renderMainMenu() {
        testContent.innerHTML = "";
        for (let test of tests) {
            testContent.appendChild(
                <div class="column is-half" onclick={() => startTest(test)}>
                    <div class="notification">
                        <h1 class="title has-text-black is-5">{test.name}</h1>
                    </div>
                </div>);
        }
    }
    function renderTestView() {
        backButton = <button class="button"
            onclick={renderMainMenu}>
            Zurück
	</button>;
        let buttonWrapper = <div class="column is-full">{backButton}</div>;
        questionContainer = <h1 class="title has-text-black is-5" />;
        answersContainer = <div />;

        let newContent = <div class="columns is-multiline">
            {buttonWrapper}
            <div class="column is-half">
                <div class="notification">
                    {questionContainer}
                </div>
            </div>
            <div class="column is-half">
                <div class="notification">
                    {answersContainer}
                    <div class="buttons is-right">
                        <button class="button is-dark"
                            onclick={chooseOption}>
                            Bestätigen
			</button>
                    </div>
                </div>
            </div>
        </div>;

        testContent.replaceWith(newContent);
        testContent = newContent;
    }

    function showQuestion(question: Question) {
        questionContainer.innerText = question.text;
        answersContainer.innerHTML = question.answers
            .map((a, i) => `<p class="control"><label class="radio has-text-black">
  <input type="radio" name="answer" value="${i}">
  ${a.text}
</label></p>`)
            .join("");
    }

    function startTest(test: PersonalityTest) {
        currentTest = test;
        currentAnswers = [];
        // Build test view
        renderTestView();
        currentQuestionIndex = 0;
        currentQuestion = currentTest.questions[currentQuestionIndex];
        showQuestion(currentQuestion);
    }

    function loadSavedTest(data: GameData) {
        let testsFound = tests.filter(t => t.name == data.testName);
        if (!testsFound) {
            renderMainMenu();
            return;
        }
        let test = testsFound[0];
        // Test restoring logic
    }

    function nextQuestion() {
        currentQuestionIndex++;
        currentQuestion = currentTest.questions[currentQuestionIndex];
        showQuestion(currentQuestion);
    }
    function chooseOption() {
        console.log("Choose clicked");
        let selectedElem = $("input[name=answer]:checked").item(0) as HTMLInputElement;
        if (selectedElem == null) {
            // TODO: Tell the user to select something
            console.log("No answer selected");
        } else {
            // Implement logic for going further here
            console.log("Chosen answer: " + selectedElem.value);
            currentAnswers.push(Number(selectedElem.value));
            nextQuestion();
        }
    }

    document.addEventListener("DOMContentLoaded", init, false);
}
