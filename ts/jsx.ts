function jsx(tag: string, attrs: { [key: string]: any }, ...children: HTMLElement[]) {
    var element = document.createElement(tag);
    for (let name in attrs) {
        if (name && attrs.hasOwnProperty(name)) {
            let value = attrs[name];
            if (value === true) {
                element.setAttribute(name, name);
            } else if (value !== false && value != null) {
                if (typeof value == 'function' && name.indexOf("on") == 0)
                    element.addEventListener(name.substr(2), value);
                else
                    element.setAttribute(name, value.toString());
            }
        }
    }
    for (let child of children) {
        element.appendChild(
            child.nodeType == null
                ? document.createTextNode(child.toString())
                : child
        );
    }
    return element;
}