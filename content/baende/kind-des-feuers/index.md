+++
title = "Kind des Feuers"
description = "Der erste Band der Reihe."
date = 2020-02-14

[extra]
include_meta = true
+++

<div class="columns is-multiline">
<div class="column is-full">
<img src="/cover.jpg" style="float: left; margin-right: 15px; margin-bottom: 10px" />

***Was sagt dir dein inneres Feuer?***

Feuer bedeutet Zuhause. Feuer bedeutet Schutz. Feuer bedeutet Hoffnung.

Seit Monica denken kann, hat Feuer auf sie eine anziehende und beruhigende Wirkung.
In ihren Träumen redet es sogar mit ihr. Doch verstehen tut sie es nie.

Das alles ändert sich, als Monica eine unverhoffte Begegnung mit einem magischen
Wesen hat, das zu allem Überfluss auch noch behauptet, sie zu kennen.

Durch dieses Ereignis scheint Monica ihrem inneren Feuer noch näher gekommen
zu sein, was von dunklen Mächten nicht unbemerkt bleibt. Kurzerhand findet sie
sich Hals über Kopf in Draconica, einer fremden Welt voller magischer Geschöpfe,
wieder.

Dies ist ihre Geschichte über Feuer, Freundschaft und Abenteuer, aber vor allem – Drachen.

<hr>

Danke an alle Leser, die das Buch gelesen und mir geholfen haben, einen guten Start hinzulegen! Danke auch an alle, die mich in Zukunft noch weiter unterstützen und meine Bücher lesen werden. Ihr seid etwas ganz Besonderes!

</div> <!-- First column -->

<div class="column columns">
	<div class="column">
		<div id="twsShopWidget_02650331_print" class="twsShopWidget"></div><script type="text/javascript">if(typeof checkLibExist == "undefined"){var script = document.createElement("script");script.src ="//www.twentysix.de/public/js/twentysix/shopWidget.min.js?123";script.type = "text/javascript";document.head.appendChild(script);var checkLibExist = true;} if(typeof books === "undefined") var books=[];books.push({"objID":"02650331","swKey":"445fdbd7a768d3ea35f90682a836a579","type":"print","size":"small","font":"nonSerif","shadow":false,"contour":true,"coverContour":true,"fontColor":"#0a0a0a","contourColor":"#d37a1c","shadowBtn":false,"contourBtn":false,"bgColor":"#dc8a25","btnFontColor":"#ffffff","btnColor":"#8f0d57","btnContourColor":"8f0d57","shop":"","bookSampleLinkText":"Jetzt probelesen","descriptionTitle":"Beschreibung","mandantShopUrl":"https://www.twentysix.de/shop","btnText":"ZUM SHOP","errMsg1Obj":"Dieser Titel ist leider<br>nicht länger verfügbar.","errMsg2Obj":"Aber im TWENTYSIX Shop gibt es viele weitere spannende Titel zu entdecken!","errMsg1Server":"Dieser Titel ist derzeit<br>leider nicht verfügbar.","errMsg2Server":"Bitte versuchen Sie es<br>später noch einmal.","errMsg3Server":"TITEL DERZEIT NICHT VERFÜGBAR"});</script>
	</div>
	<div class="column is-full">
		<div id="twsShopWidget_02650331_ebook" class="twsShopWidget"></div><script type="text/javascript">if(typeof checkLibExist == "undefined"){var script = document.createElement("script");script.src ="//www.twentysix.de/public/js/twentysix/shopWidget.min.js?123";script.type = "text/javascript";document.head.appendChild(script);var checkLibExist = true;} if(typeof books === "undefined") var books=[];books.push({"objID":"02650331","swKey":"445fdbd7a768d3ea35f90682a836a579","type":"ebook","size":"small","font":"nonSerif","shadow":false,"contour":true,"coverContour":true,"fontColor":"#0a0a0a","contourColor":"#d37a1c","shadowBtn":false,"contourBtn":false,"bgColor":"#dc8a25","btnFontColor":"#ffffff","btnColor":"#8f0d57","btnContourColor":"8f0d57","shop":"","bookSampleLinkText":"Jetzt probelesen","descriptionTitle":"Beschreibung","mandantShopUrl":"https://www.twentysix.de/shop","btnText":"ZUM SHOP","errMsg1Obj":"Dieser Titel ist leider<br>nicht länger verfügbar.","errMsg2Obj":"Aber im TWENTYSIX Shop gibt es viele weitere spannende Titel zu entdecken!","errMsg1Server":"Dieser Titel ist derzeit<br>leider nicht verfügbar.","errMsg2Server":"Bitte versuchen Sie es<br>später noch einmal.","errMsg3Server":"TITEL DERZEIT NICHT VERFÜGBAR"});</script>
	</div>
</div>

**Dragon Tale - Kind des Feuers** bei: **[Thalia](https://www.thalia.de/shop/home/artikeldetails/ID141690024.html)** |
**[Hugendubel](https://www.hugendubel.de/de/buch/aylin_hacker-dragon_tale_kind_des_feuers-35414650-produkt-details.html)** |
**[Morawa](https://morawa.at/detail/ISBN-9783740753306/Hacker-Aylin/Dragon-Tale---Kind-des-Feuers)** |
**[Amazon](https://www.amazon.de/Dragon-Tale-Feuers-Aylin-Hacker/dp/3740753307)**
