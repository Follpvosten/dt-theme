+++
title = "Herz des Drachen"
description = "Der zweite Band der Reihe."
date = 2020-02-13

[extra]
include_meta = true
+++

<div class="columns is-multiline">
<div class="column is-full">
<img src="/cover2.jpg" style="float: left; margin-right: 15px; margin-bottom: 10px" />

***Was wirst du tun, wenn dein inneres Feuer erwacht?***

Nachdem Monica sich an ihr neues Leben als Prinzessin gewöhnt hat, überschlagen
sich die Ereignisse erneut und eine alte Bedrohung kehrt schleichend zurück.
Doch auch abseits von Draconica läuft nicht alles glatt.

Als wäre das nicht genug, entdeckt Monica eine neue Seite an sich, die droht,
die Kontrolle zu übernehmen. Ihr inneres Feuer scheint ein ganz eigenes Leben
führen zu wollen und es gibt jemanden, der behauptet, mehr zu wissen.
Jemanden, den Monica niemals dachte, wiederzusehen.

Monicas Geschichte geht weiter – ihre Geschichte über
Feuer, Freundschaft und Abenteuer, aber vor allem – Drachen.

<hr>

Ganz besonderer Dank gilt meiner neuen Korrektorin, die diesen Band
mit viel Gewissen und Professionalität durchgegangen ist, um das
Gesamtwerk noch besser zu machen.

Vielen Dank! Bitte schaut bei <strong>[Niquinja](http://niquinjas-korrektorat.com/)</strong> vorbei!

</div> <!-- First columns -->

<div class="column columns">
	<div class="column">
		<div id="twsShopWidget_02984039_print" class="twsShopWidget"></div><script type="text/javascript">if(typeof checkLibExist == "undefined"){var script = document.createElement("script");script.src ="//www.twentysix.de/public/js/twentysix/shopWidget.min.js?123";script.type = "text/javascript";document.head.appendChild(script);var checkLibExist = true;} if(typeof books === "undefined") var books=[];books.push({"objID":"02984039","swKey":"3600546cb66055d2364ae18f32887902","type":"print","size":"small","font":"nonSerif","shadow":false,"contour":true,"coverContour":true,"fontColor":"#0a0a0a","contourColor":"#d37a1c","shadowBtn":false,"contourBtn":false,"bgColor":"#dc8a25","btnFontColor":"#ffffff","btnColor":"#8f0d57","btnContourColor":"8f0d57","shop":"","bookSampleLinkText":"Jetzt probelesen","descriptionTitle":"Beschreibung","mandantShopUrl":"https://www.twentysix.de/shop","btnText":"ZUM SHOP","errMsg1Obj":"Dieser Titel ist leider<br>nicht länger verfügbar.","errMsg2Obj":"Aber im TWENTYSIX Shop gibt es viele weitere spannende Titel zu entdecken!","errMsg1Server":"Dieser Titel ist derzeit<br>leider nicht verfügbar.","errMsg2Server":"Bitte versuchen Sie es<br>später noch einmal.","errMsg3Server":"TITEL DERZEIT NICHT VERFÜGBAR"});</script>
	</div>
	<div class="column is-full">
		<div id="twsShopWidget_02984039_ebook" class="twsShopWidget"></div><script type="text/javascript">if(typeof checkLibExist == "undefined"){var script = document.createElement("script");script.src ="//www.twentysix.de/public/js/twentysix/shopWidget.min.js?123";script.type = "text/javascript";document.head.appendChild(script);var checkLibExist = true;} if(typeof books === "undefined") var books=[];books.push({"objID":"02984039","swKey":"3600546cb66055d2364ae18f32887902","type":"ebook","size":"small","font":"nonSerif","shadow":false,"contour":true,"coverContour":true,"fontColor":"#0a0a0a","contourColor":"#d37a1c","shadowBtn":false,"contourBtn":false,"bgColor":"#dc8a25","btnFontColor":"#ffffff","btnColor":"#8f0d57","btnContourColor":"8f0d57","shop":"","bookSampleLinkText":"Jetzt probelesen","descriptionTitle":"Beschreibung","mandantShopUrl":"https://www.twentysix.de/shop","btnText":"ZUM SHOP","errMsg1Obj":"Dieser Titel ist leider<br>nicht länger verfügbar.","errMsg2Obj":"Aber im TWENTYSIX Shop gibt es viele weitere spannende Titel zu entdecken!","errMsg1Server":"Dieser Titel ist derzeit<br>leider nicht verfügbar.","errMsg2Server":"Bitte versuchen Sie es<br>später noch einmal.","errMsg3Server":"TITEL DERZEIT NICHT VERFÜGBAR"});</script>
	</div>
</div>

**Dragon Tale - Herz des Drachen** bei: **[Thalia](https://www.thalia.de/shop/home/artikeldetails/ID146952614.html)** |
**[Hugendubel](https://www.hugendubel.de/de/buch_gebunden/aylin_hacker-dragon_tale_herz_des_drachen-38572734-produkt-details.html)** |
**[Morawa](https://morawa.at/detail/ISBN-9783740763695/Hacker-Aylin/Dragon-Tale---Herz-des-Drachen)** |
**[Amazon](https://www.amazon.de/Dragon-Tale-Drachen-Aylin-Hacker/dp/3740763698)**
