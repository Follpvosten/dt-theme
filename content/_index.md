+++
title = "Home"
template = "index.html"
+++

<img src="cover.jpg" style="float: left; margin-right: 15px; margin-bottom: 10px" />

***Was sagt dir dein inneres Feuer?***

Willkommen in Draconica, dem Land der Drachen!

Du liebst Fantasy, Abenteuer und Mysterien?
Geschichten, die dich fesseln und zum Träumen einladen?

Du tauchst gerne in fremde Welten ein und würdest am liebsten sofort deine
Koffer packen und dort leben?

Du stehst auf actionreiche Kämpfe und spannende Handlungen, aber auch auf
ruhige und entspannende Passagen?

Du liebst Drachen und alles, was mit Mystik und Fabelwesen zusammenhängt
(aber vor allem Drachen)?

Du magst Charaktere, die liebenswert sind und auch einen kleinen Sprung in der
Schüssel haben, du aber von der ersten Sekunde an ins Herz schließt?

Dann gib Dragon Tale eine Chance und lass dich auf ein völlig neues Abenteuer ein!

Falls du aber noch Bedenkzeit und Überzeugung brauchst, klick dich ruhig ein
wenig auf dieser Seite umher. Viel wird noch nicht verraten, einiges bleibt noch
ein Geheimnis, aber es sollte schon einmal ein Leckerbissen auf das sein,
was dich erwartet.

Und jetzt breite deine Schwingen aus und erkunde Draconica.

<div class="columns">
	<div class="column">
		<div id="twsShopWidget_02650331_print" class="twsShopWidget"></div><script type="text/javascript">if(typeof checkLibExist == "undefined"){var script = document.createElement("script");script.src ="//www.twentysix.de/public/js/twentysix/shopWidget.min.js?123";script.type = "text/javascript";document.head.appendChild(script);var checkLibExist = true;} if(typeof books === "undefined") var books=[];books.push({"objID":"02650331","swKey":"445fdbd7a768d3ea35f90682a836a579","type":"print","size":"small","font":"nonSerif","shadow":false,"contour":true,"coverContour":true,"fontColor":"#0a0a0a","contourColor":"#d37a1c","shadowBtn":false,"contourBtn":false,"bgColor":"#dc8a25","btnFontColor":"#ffffff","btnColor":"#8f0d57","btnContourColor":"8f0d57","shop":"","bookSampleLinkText":"Jetzt probelesen","descriptionTitle":"Beschreibung","mandantShopUrl":"https://www.twentysix.de/shop","btnText":"ZUM SHOP","errMsg1Obj":"Dieser Titel ist leider<br>nicht länger verfügbar.","errMsg2Obj":"Aber im TWENTYSIX Shop gibt es viele weitere spannende Titel zu entdecken!","errMsg1Server":"Dieser Titel ist derzeit<br>leider nicht verfügbar.","errMsg2Server":"Bitte versuchen Sie es<br>später noch einmal.","errMsg3Server":"TITEL DERZEIT NICHT VERFÜGBAR"});</script>
	</div>
	<div class="column is-full">
		<div id="twsShopWidget_02650331_ebook" class="twsShopWidget"></div><script type="text/javascript">if(typeof checkLibExist == "undefined"){var script = document.createElement("script");script.src ="//www.twentysix.de/public/js/twentysix/shopWidget.min.js?123";script.type = "text/javascript";document.head.appendChild(script);var checkLibExist = true;} if(typeof books === "undefined") var books=[];books.push({"objID":"02650331","swKey":"445fdbd7a768d3ea35f90682a836a579","type":"ebook","size":"small","font":"nonSerif","shadow":false,"contour":true,"coverContour":true,"fontColor":"#0a0a0a","contourColor":"#d37a1c","shadowBtn":false,"contourBtn":false,"bgColor":"#dc8a25","btnFontColor":"#ffffff","btnColor":"#8f0d57","btnContourColor":"8f0d57","shop":"","bookSampleLinkText":"Jetzt probelesen","descriptionTitle":"Beschreibung","mandantShopUrl":"https://www.twentysix.de/shop","btnText":"ZUM SHOP","errMsg1Obj":"Dieser Titel ist leider<br>nicht länger verfügbar.","errMsg2Obj":"Aber im TWENTYSIX Shop gibt es viele weitere spannende Titel zu entdecken!","errMsg1Server":"Dieser Titel ist derzeit<br>leider nicht verfügbar.","errMsg2Server":"Bitte versuchen Sie es<br>später noch einmal.","errMsg3Server":"TITEL DERZEIT NICHT VERFÜGBAR"});</script>
	</div>
</div>
<div class="columns">
	<div class="column">
		<div id="twsShopWidget_02984039_print" class="twsShopWidget"></div><script type="text/javascript">if(typeof checkLibExist == "undefined"){var script = document.createElement("script");script.src ="//www.twentysix.de/public/js/twentysix/shopWidget.min.js?123";script.type = "text/javascript";document.head.appendChild(script);var checkLibExist = true;} if(typeof books === "undefined") var books=[];books.push({"objID":"02984039","swKey":"3600546cb66055d2364ae18f32887902","type":"print","size":"small","font":"nonSerif","shadow":false,"contour":true,"coverContour":true,"fontColor":"#0a0a0a","contourColor":"#d37a1c","shadowBtn":false,"contourBtn":false,"bgColor":"#dc8a25","btnFontColor":"#ffffff","btnColor":"#8f0d57","btnContourColor":"8f0d57","shop":"","bookSampleLinkText":"Jetzt probelesen","descriptionTitle":"Beschreibung","mandantShopUrl":"https://www.twentysix.de/shop","btnText":"ZUM SHOP","errMsg1Obj":"Dieser Titel ist leider<br>nicht länger verfügbar.","errMsg2Obj":"Aber im TWENTYSIX Shop gibt es viele weitere spannende Titel zu entdecken!","errMsg1Server":"Dieser Titel ist derzeit<br>leider nicht verfügbar.","errMsg2Server":"Bitte versuchen Sie es<br>später noch einmal.","errMsg3Server":"TITEL DERZEIT NICHT VERFÜGBAR"});</script>
	</div>
	<div class="column is-full">
		<div id="twsShopWidget_02984039_ebook" class="twsShopWidget"></div><script type="text/javascript">if(typeof checkLibExist == "undefined"){var script = document.createElement("script");script.src ="//www.twentysix.de/public/js/twentysix/shopWidget.min.js?123";script.type = "text/javascript";document.head.appendChild(script);var checkLibExist = true;} if(typeof books === "undefined") var books=[];books.push({"objID":"02984039","swKey":"3600546cb66055d2364ae18f32887902","type":"ebook","size":"small","font":"nonSerif","shadow":false,"contour":true,"coverContour":true,"fontColor":"#0a0a0a","contourColor":"#d37a1c","shadowBtn":false,"contourBtn":false,"bgColor":"#dc8a25","btnFontColor":"#ffffff","btnColor":"#8f0d57","btnContourColor":"8f0d57","shop":"","bookSampleLinkText":"Jetzt probelesen","descriptionTitle":"Beschreibung","mandantShopUrl":"https://www.twentysix.de/shop","btnText":"ZUM SHOP","errMsg1Obj":"Dieser Titel ist leider<br>nicht länger verfügbar.","errMsg2Obj":"Aber im TWENTYSIX Shop gibt es viele weitere spannende Titel zu entdecken!","errMsg1Server":"Dieser Titel ist derzeit<br>leider nicht verfügbar.","errMsg2Server":"Bitte versuchen Sie es<br>später noch einmal.","errMsg3Server":"TITEL DERZEIT NICHT VERFÜGBAR"});</script>
	</div>
</div>

## Dragon Tale in verschiedenen Online-Shops
**Dragon Tale - Kind des Feuers** bei: **[Thalia](https://www.thalia.de/shop/home/artikeldetails/ID141690024.html)** |
**[Hugendubel](https://www.hugendubel.de/de/buch/aylin_hacker-dragon_tale_kind_des_feuers-35414650-produkt-details.html)** |
**[Morawa](https://morawa.at/detail/ISBN-9783740753306/Hacker-Aylin/Dragon-Tale---Kind-des-Feuers)** |
**[Amazon](https://www.amazon.de/Dragon-Tale-Feuers-Aylin-Hacker/dp/3740753307)**

**Dragon Tale - Herz des Drachen** bei: **[Thalia](https://www.thalia.de/shop/home/artikeldetails/ID146952614.html)** |
**[Hugendubel](https://www.hugendubel.de/de/buch_gebunden/aylin_hacker-dragon_tale_herz_des_drachen-38572734-produkt-details.html)** |
**[Morawa](https://morawa.at/detail/ISBN-9783740763695/Hacker-Aylin/Dragon-Tale---Herz-des-Drachen)** |
**[Amazon](https://www.amazon.de/Dragon-Tale-Drachen-Aylin-Hacker/dp/3740763698)**
