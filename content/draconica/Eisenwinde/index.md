+++
title = "Eisenwinde"
description = "Die eisige Insel ist das sechste Gebiet Draconicas."
date = 2019-01-13

[extra]
map = { image="karte-eisenwinde.png", text="Karte von Eisenwinde" }
gallery = [
	{ path="flagge.png", text="Die Flagge des Gebiets" },
	{ path="banner.png", text="Das Banner der Königsfamilie" }
]
+++

***"Es ist kalt. Dort liegt Schnee und Eis. Gletscher. Gletscher überall. Warum ist es so kalt?"***

Die Insel Eisenwinde ist das sechste Gebiet Draconicas und wurde vom Portalbeschwörer
Santiago erschaffen.

Das Portal liegt in einem Gletscher in Gletscher 2 und führt ins Feuerland, Südamerika.

Das Gebiet zeichnet sich vor allem durch seine kalten Temperaturen und Schneestürme
mit sehr viel Schneefall aus. Es ist allgemein eine sehr raue Gegend. Die Landschaft
besteht nur aus Eis und Schnee und ist von Gletschern gekennzeichnet.

## Bezirke
* Gletscher 1
  * Hier steht der Palast von Eisenwinde.
* Gletscher 2
  * Hier steht das Portal.
  * Außerdem lebt hier die Pinguinkolonie.
  * Das Barbarenlager kann man in der Nähe des Portals finden.
* Gletscher 3
  * Am Meer liegt die Wassertaxi-Station.
* Akropyen
  * Ein geheimnisvoller Bezirk, zu dem der Zutritt strengstens untersagt ist.

## Die Königsfamilie Laszuli
***"Mir ist kalt."***

Zu dieser Königsfamilie zählen Königin Koko, Prinzessin Lapis und Leibwächter
Eiszahn.

## Trivia
* Hier ist es sehr kalt. Das ist auch dem Portalbeschwörer aufgefallen.
