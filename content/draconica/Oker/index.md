+++
title = "Oker"
description = "Das zweite Gebiet Draconicas und eine Wüsten-Inselgruppe."
date = 2019-01-13

[extra]
map = { image="karte-oker.png", text="Karte von Oker" }
gallery = [
	{ path="flagge.png", text="Die Flagge des Gebiets" },
	{ path="banner.png", text="Das Banner der Königsfamilie" }
]
+++

***"Unbändige Hitze, freies Herz, spring heraus und brüll', Löwe!
Mysterien erwarten den Suchenden, wenn du auf die Farben des Lebens blickst,
erspähst du sie, dich leitend in den Kampf."***

Oker ist das zweite Gebiet Draconicas und eine Wüsten-Inselgruppe, die vom
Portalbeschwörer Mpho erschaffen wurde.

Das Portal von Oker befindet sich in einer kryptischen Wandmalerei in einer
alten Ruine im Bezirk Mnyama und führt in die Drakensberge von Swasiland.

Oker zeichnet sich vor allem durch seine Wüsten aus, die die gesamte Landfläche
einnehmen. Dennoch gibt es eine riesige Metropole, die in der Wüste heraussticht,
und viele kleinere Oasen.

## Bezirke
* Mnyama
  * Das größte Gebiet; hier befinden sich der Palast und das Portal, sowie
	Taros Dorf.
* Golide
  * Die oben erwähnte große Metropole, die durch ihre blinkenden Lichter und
	fortschrittliche Infrastruktur geprägt ist.
* Mhlophe
  * Hier befindet sich in einem kleinen Fischerdorf am Meer die Anlegestelle des
	Wassertaxis.
* Mbovu
  * Der gefährlichste Bezirk und tiefste Punkt der Wüste, an den man sich nicht
	verirren sollte. Die herumliegenden Drachenknochen sind der Beweis.  
	Hier herrschen die höchsten Temperaturen in ganz Draconica.
* Mtfubi, Luhlata Sasibhakabhaka, Luhlata
  * Sind auch unter dem Namen "Farbinseln" bekannt, da sie teilweise bunte
	Sandfärbungen aufweisen.

## Die Königsfamilie Omolowo-Eboh
***"Jedes einzelne Sandkorn erzählt eine Geschichte, die uns das Leben weist."***

Das Königshaus von Oker besteht derzeit nur aus Prinzessin Sonora Omolowo-Eboh
und ihrer Leibwächterin Anita.

## Trivia
* Die Bezirke von Oker sind nach Namen von Farben in der Siswati-Sprache
  benannt.
* Es ist fast unmöglich, irgendetwas in dieser Sprache zu recherchieren.
  Echt jetzt.
