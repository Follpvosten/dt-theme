+++
title = "Flügelklippe"
description = "Das erste Gebiet Draconicas, das von Lavasturm erschaffen wurde."
date = 2019-01-13

[extra]
map = { image="karte-fluegelklippe.png", text="Karte von Flügelklippe" }
gallery = [
	{ path="flagge.png", text="Die Flagge des Gebiets" },
	{ path="banner.png", text="Das Banner der Königsfamilie" }
]
+++

***"Flieg‘ hoch hinaus und küsse die Wolken, berühre die Sonne, vereine dich mit
den Sternen, halte den Mond in deinen Krallen."***

Die Flügelklippe war das erste Gebiet, das von Lavasturm erschaffen wurde. Sie
liegt im Südosten unter Blauküste und grenzt an Sturm und Redemia.

Das Portal führt durch eine Diamantsäule auf dem Friedhof im Dorfe vor dem
Palast in die Kanalisation von New York City, USA.

Das Gebiet ist stark durch seine weiten Wiesen und Hügellandschaften geprägt.
Es gibt jedoch auch ein kleineres, nicht allzu dichtes Waldgebiet.  
Ein bekannter Fluss namens Abflussrohr fließt durch die Flügelklippe.

## Bezirke
* Plumogia
  * Hier befinden sich der Palast und das Portal, sowie die namensgebende Klippe.
* Remigia
  * Dort befindet sich das kleine Waldgebiet, durch das das Abflussrohr fließt.
  * Hier steht das Haus von Elfenflug und Grünbart.
* Rectricia
  * Hier 'findet sich die Puimuhchen-Farm (mit der besten Milch in ganz Draconica!)
* Tectricia
  * Grenzt an Sturm und Redemia, wodurch aus dieser Richtung öfter starke Sturmböen
	über's Flachland blasen.
* Pennigia
  * Ein vom Fluss geprägtes Gebiet; die Bewohner hier leben größtenteils am Wasser
	und betreiben Fischerei.

## Die Königsfamilie Luna Moon da Lilly del Sol
***"Entfache die Flamme in dir und finde den Weg des Drachen."***

Zu Beginn der Geschichte gilt diese Familie als ausgestorben; es findet sich kein
williger Thronfolger.

Dennoch wird der Palast vom königlichen Berater Donnerzahn am Laufen gehalten,
ohne dessen staatlich geprüften Zeitplan alles den Bach runtergehen würde.

## Trivia
* Die Bezirke der Flügelklippe wurden nach Begriffen aus der Plumologie, der
  Wissenschaft, die sich mit Federn auseinandersetzt, benannt.

