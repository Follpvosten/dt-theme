+++
title = "Draconica"
description = "In dieser Fantasy-Welt findet der Hauptteil der Handlung statt."
sort_by = "date"

[extra]
map = { image = "./karte-draconica.png", text = "Karte von Draconica" }
+++

Draconica, das Land der Drachen, existiert schon seit Urzeiten. Der erste
Drache, Lavasturm, gründete damit einen Zufluchtsort für allerlei magische
Wesen, jedoch hauptsächlich für seinesgleichen, an dem sie in Frieden leben
konnten.

Er erschuf das erste Gebiet, die Flügelklippe, die jedoch bald nicht mehr genug
Platz für die anwachsende Bevölkerung Draconicas bot.

Lavasturm begab sich auf die Suche nach würdigen Anführern auf der Erde, die
magische Fähigkeiten besaßen, wie kein anderer, um diesen in einer Vision zu
erscheinen und ihnen ihre wahre Bestimmung als Portalbeschwörer zu offenbaren.

Diese haben die Aufgabe, in Draconica ein neues Gebiet an die schon bestehenden
anzuhängen, welches sie teilweise nach ihren Wünschen und Vorstellungen, jedoch
auch nach ihrem Gemüt formen können.

Durch die ganze Weltgeschichte hindurch entstanden nach und nach immer mehr
Gebiete in Draconica.

Das passierte immer wieder, bis vor einigen Jahren, als die geheimnisvolle Insel
auftauchte...

# Gebiete Draconicas
