+++
title = "Donnerhall"
description = "Ein raues Berggebiet und das dritte Draconicas."
date = 2019-01-13

[extra]
map = { image="karte-donnerhall.png", text="Karte von Donnerhall" }
gallery = [
	{ path="flagge.png", text="Die Flagge des Gebiets" },
	{ path="banner.png", text="Das Banner der Königsfamilie" }
]
+++

***"Einzigartig, wie jeder Berg, bilden wir doch eine Einheit.
Glühend heiße Vulkane, weiße Berge geschmückt durch Schnee,
felsige, raue Gebirge stählern unsere Seele für die Zukunft
und lassen die Vergangenheit hinter uns."***

Donnerhall ist ein raues Berggebiet und war das dritte Gebiet Draconicas, das
von der Portalbeschwörerin Lowanna erschaffen wurde.

Das Portal befindet sich hier im aktiven Vulkan Felswerfer im Bezirk Vulkagma
und führt zum Uluru in Australien.

Auffällig in Donnerhall sind die bergigen Gegenden, deren Erscheinungsbild
dennoch variiert. Hier findet man von winterlichen Skigebieten bis hin zu
brodelnden Lavakesseln alles, was man sich unter Bergen oder Vulkanen
vorstellen kann.

## Bezirke
* Hohenberg
  * Hier steht der Palast. Der Bezirk zeichnet sich vor allem durch seine
	spitzen Berge und die tiefen Täler aus, sowie die dichten Wälder, die
	unterhalb der Baumgrenze in Meernähe wachsen.
* Vulkagma
  * In dieser von Vulkanen geprägten Landschaft befindet sich das Portal im
	aktiven Vulkan Felswerfer.
  * Ein Fluss aus Lava zieht sich durch diesen Bezirk, der Vulkanpfad genannt
	wird und im Dämonenberg endet.
* Bergtal
  * Diese raue, unfreundliche Gegend ist für durchgehende Berge bekannt. Hier
	wird noch Bergbau betrieben.
* Winterfell
  * Dieser Bezirk erfreut sich bei Touristen großer Beliebtheit, da es hier
	fantastische Skigebiete sowie saftige Almen gibt.
  * Der Fluss Donnerbach führt bis zum heiligen Gebirge, das aus den drei
	Bergen Lichtfelsen, Windgipfel und Schneebrocken besteht.

## Die Königsfamilie Monteno
***"Wenn du deinen Weg gehst, sieh den Berg vor dir nicht als Hürde,
sondern als Möglichkeit, zu wachsen."***

Die Königsfamilie Monteno besteht aus Königin Diana, König Ludger, Prinzessin
Lucia und Prinz Sven, sowie Leibwächter Dirk Autentahl.

## Trivia
* Vulkagma ist ein dummes Wortspiel mit den Worten "Vulkan" und "Magma".
  Was ich mir dabei gedacht habe, weiß ich nicht.
