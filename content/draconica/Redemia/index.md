+++
title = "Redemia"
description = "Das Hauptzentrum von Draconica ist dessen viertes Gebiet."
date = 2019-01-13

[extra]
map = { image="karte-redemia.png", text="Karte von Redemia" }
gallery = [
	{ path="flagge.png", text="Die Flagge des Gebiets" },
	{ path="banner.png", text="Das Banner der Königsfamilie" }
]
+++

***"Kein Krieg und kein Tod belastet uns, wenn wir zum Himmel blicken und träumen.
Mögen wir den Zusammenhalt finden, der uns die Harmonie zeigt, in der wir leben."***

Redemia ist das vierte Gebiet von Draconica, welches vom Portalbeschwörer Hideyoshi
erschaffen wurde.

Das Portal von Redemia befindet sich in der Nordstadt auf einer Werbetafel und
führt zur Tosa-Bucht in der Präfektur Kōchi in Japan.

Redemia besteht größtenteils aus einer riesigen Großstadt, die die meisten
Bezirke einnimmt. Es gibt jedoch auch kleinere Dörfer und Bauernhöfe im
Außenring.

## Bezirke
* Palaststadt
  * Der Name ist Programm; ein Hügel, auf dessen Spitze der Palast steht.
* Nordstadt
  * Das Vergügungsviertel. Hier gibt es hauptsächlich Bars, Clubs und
	Shopping-Möglichkeiten wie das Vulcano Center, sowie den berühmten
	Freizeitpark Magic Vulcano, der gleich daneben liegt.
  * Hier befindet sich ebenfalls das Portal.
* Oststadt
  * Auch unter dem Namen Ruhezone bekannt. Hier gibt es viele Möglichkeiten,
	um vom stressigen Stadtleben Abstand zu nehmen und viele Seen, Parks und
	Gärten zu besichtigen.
  * In diesem Gebiet leben hauptsächlich die älteren Draconicaner.
* Südstadt
  * Auch als Kulturviertel bekannt. Hier findet man vor allem einzigartige
	Architektur und Sehenswürdigkeiten.
  * Hier befindet sich auch der Drachenexpress-Bahnhof und der Luftschiffhafen.
* Weststadt
  * Dieser Teil von Redemia, auch als Altstadt oder Wohngebiet bekannt, ist der
	am meisten bewohnte Teil Redemias.
* Außenring
  * Hier finden sich die Felder und Bauernhöfe. Es wird Landwirtschaft betrieben.

## Die Königsfamilie Nephirit
***"Höre auf das Seelenfeuer in dir."***

Dieses Königshaus besteht aus König Houston, Prinz Jayde und Leibwächterin
Pamilla.
