+++
title = "Grünewald"
description = "Ein großes Waldgebiet und das fünfte Gebiet Draconicas."
date = 2019-01-13

[extra]
map = { image="karte-gruenewald.png", text="Karte von Grünewald" }
gallery = [
	{ path="flagge.png", text="Die Flagge des Gebiets" },
	{ path="banner.png", text="Das Banner der Königsfamilie" }
]
+++

***"Grenzenlose Fantasie,
jung und frisch wie der Frühling,
warm und besonnener Sommer,
bunt und fröhlich im Herbst,
gemütlich und verständnisvoll der Winter."***

Grünewald ist das fünfte Gebiet Draconicas und wurde von der Portalbeschwörerin
Ines geschaffen.

Das Portal befindet sich in einem großen Nussbaum namens Baumhardt und führt in
das kleine Wäldchen neben Monicas Dorf in Deutschland.

Grünewald ist, wie der Name schon vermuten lässt, ein einziges, zusammenhängendes
Waldgebiet. Die Besonderheit ist, dass in jedem Bezirk eine andere Jahreszeit
herrscht.

## Bezirke
* Ulmenblatt
  * Der bunte Herbstwald.
  * Hier liegt am Übergang zu Eichenharz das Puininchen-Dorf.
  * Außerdem lebt hier der Sprung von Nikolassedoruben, einem Hirsch und Herrscher
	des Waldes.
* Eichenharz
  * Das saftige Grün der Bäume dieses Bezirks verkündet immerwährenden Sommer.
  * Der Portalbaum Baumhardt steht ebenfalls hier.
  * Außerdem lebt hier auch der Stamm der Nusspuis, die Krieger von Grünewald.
  * Am Übergang nach Pinienholz liegt die Wassertaxi-Station.
* Pinienholz
  * Im Winterbezirk von Grünewald herrschen frostige Temperaturen.
  * Auch, wenn es hier nicht viel schneit, ergibt der glitzernde Raureif dieses
	Bezirks eine wunderschöne Winterlandschaft.
  * Ein Teil des Geistergehölzes liegt hier.
* Buchenrinde
  * In Frühlingsgebiet von Grünewald sprießen die Blumen und ein frisches
	Lüftlein weht.
  * Hier befindet sich der Palast von Grünewald.
  * Der hauptsächliche Teil des Geistergehölzes liegt in diesem Bezirk.
  * Im Süden liegt die Knusperpui-Kolonie.

## Die Königsfamilie Bellatoxic
***"Lass die Sonne für dich strahlen und du wirst dich, wie die schönste Blume, nach ihr erstrecken."***

Das Königshaus der Bellatoxics steht zur Zeit leer und wird von der königlichen
Beraterin Elea und dem Hauptmann Eduardo vertreten.

## Trivia
* Die meisten nicht mit den Drachen verwandten Fabelwesen, die in Draconica
  existieren, stammen ursprünglich aus Grünewald.
