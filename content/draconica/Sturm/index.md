+++
title = "Sturm"
description = "Das achte und momentan letzte Gebiet Draconicas ist immer stürmisch und in Nebel gehüllt."
date = 2019-01-13

[extra]
map = { image="karte-sturm.png", text="Karte von Sturm" }
gallery = [
	{ path="flagge.png", text="Die Flagge des Gebiets" },
	{ path="banner.png", text="Das Banner der Königsfamilie" }
]
+++

***"Stürmisches Wetter, tiefer Schmerz, so launisch, wie unser Gemüt.
Trotz Nebel bewahren wir eine klare Sicht auf unsere Ziele,
denn der Sinn des Lebens liegt in jedem Einzelnen von uns, der den Frieden trägt."***

Sturm ist das achte und jüngste Gebiet Draconicas, das vom Portalbeschwörer
Alexej erschaffen wurde.

Das Portal befindet sich in einem Felsen, auf den ein Bild von einem Drachen
eingeritzt ist und führt nach Minsk, Weißrussland.

Dieses Gebiet zeichnet sich besonders durch sein launisches Wetter aus, das
sich in Sekundenschnelle ändern kann. Von hügeligen Berglandschaften über
Flachland bis zu dicht bewachsenen Nadelwäldern kann man hier alles finden -
oder auch nicht finden, weil meist dichter Nebel herrscht.

## Bezirke
* Navaĺnica
  * In diesem Bezirk steht das in Draconica berühmte Wetterinstitut, wo
	Wetterforschung betrieben wird, um jährlich den einzigen sonnigen Tag in
	Sturm voraussehen zu können, an dem das Sturmfest veranstaltet wird.
* Viasiolka
  * In dieser rauen Gegend steht das Portal von Sturm.
* Tuman
  * In den tiefen Nadelwäldern dieses Bezirks lebt die Flederpui-Kolonie.
* Snieh
  * In dieser besonders nebligen Gegend ist die einzige Großstadt von Sturm,
	Livień, angesiedelt.
* Soniečnaje Sviatlo
  * Der Palast von Sturm befindet sich in diesem Waldgebiet.
* Doždž
  * In diesem besonders stürmischen Bezirk liegt das Sturmtief, eine gefährliche
	Gegend, in der angeblich eine uralte Bestie hausen soll.

## Die Königsfamilie Znichka
***"Möge der Frieden in unseren Sternen stehen und bewahrt werden."***

In diesem Königshaus leben König Horace, König Dunkelherz und ihre Kinder
Prinz Dennis und Prinzessin Giftdorn mit ihrer Leibwächterin Trudlinde.

## Trivia
* Die Bezirke von Sturm sind nach Wetterphänomenen auf Weißrussisch benannt.
