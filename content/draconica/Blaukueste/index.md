+++
title = "Blauküste"
description = "Das siebte Gebiet Draconicas, welches hauptsächlich unter Wasser liegt."
date = 2019-01-13

[extra]
map = { image="karte-blaukueste.png", text="Karte von Blauküste" }
gallery = [
	{ path="flagge.png", text="Die Flagge des Gebiets" },
	{ path="banner.png", text="Das Banner der Königsfamilie" }
]
+++

***"Tief, wie der Dschungel, wie die See mal sanft, mal wild müssen wir sein.
Hoch, wie eine Welle schlagen wir ein, um zu schützen die Kinder Draconicas."***

Blauküste ist das siebte Gebiet Draconicas, welches von der Portalbeschwörerin
Kirana erschaffen wurde.

Das Portal befindet sich in einer Landkarte, die in einem kleinen Hausboot hängt,
welches auf dem Wasser des Bezirks Stoß schwimmt. Es führt in eine alte
Tempelruine nahe Patna, Indien.

Das Gebiet besteht zu 80% Wasser, auf oder unter dem die Bewohner hauptsächlich
leben. Man findet im Norden dennoch eine große Hafenstadt und einen tiefen
Dschungel im westlichen Bezirk Arecae.

## Bezirke
* Arecae
  * Dies ist das tiefe Dschungelgebiet, in dem man sich leicht verlaufen kann.
  * Die Bäume ragen hier fast bis in den Himmel Draconicas.
* Pharagmæ
  * Das größte Strandgebiet Draconicas, das zum Baden einlädt.
  * Im Norden liegt die vielbeschäftigte Hafenstadt Port Hæven, von der viele
	Seerouten ausgehen.
* Kapillar
  * Der gefährlichste Bezirk auf dem Meer.
  * Hier schlagen die Wellen besonders hoch, weswegen sich nur erfahrene
	Wasserdrachen und Seemänner hierher trauen.
* Tsunami
  * Im größten Bezirk von Blauküste liegt der Palast unter der Wasseroberfläche.
* Schwere
  * Im Süden dieses Bezirks liegt die Strudellagune, in der die weise
	Geschichtenerzählerin Wasserstern lebt.
* Stoß
  * In diesem Bezirk treibt das Hausboot mit dem Portal auf den ruhigen Wellen.

## Die Königsfamilie Anker-Hotok
***"Du allein bestimmst den Seegang deines Lebens."***

Von diesem Königshaus ist nur noch der König Strudelauge übrig geblieben.

## Trivia
* Die Wasserbezirke sind nach verschiedenen Wellenarten benannt, während
  Arecae vom wissenschaftlichen Namen für Palmengewächse inspiriert ist.
