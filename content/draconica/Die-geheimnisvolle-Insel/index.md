+++
title = "Die geheimnisvolle Insel"
description = "Eine rätselhafte, unerkundete Insel im Nordwesten Draconicas."
date = 2019-01-12
+++

Über diese Insel ist noch nicht viel bekannt, was daran liegt, dass sie von
einer dunklen Mauer aus Magie beschützt wird. Was dahinter liegt und was die
Ursachen sind, ist noch unklar. Der Portalbeschwörer hat bisher noch nicht
versucht, Kontakt mit der Außenwelt aufzunehmen, und sich tief im Inneren der
Insel verschanzt.

Es ist weder etwas über den Portalbeschwörer selbst, noch etwas über den Standort
des Portals auf der Erde bekannt.

Was verbirgt sich auf dieser Insel?
