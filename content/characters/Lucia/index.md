+++
title = "Lucia"
description = "Die Prinzessin von Donnerhall."
weight = 3
+++

***"Heiliger Drachenkuchen!"***

Lucia ist die Thronfolgerin des Gebiets Donnerhall, die zu Beginn der
Geschichte unter einem fehlgeschlagenen Zauber leidet.

## Steckbrief
* Alter: 14 Jahre
* Geburtstag: 3. Dezember
* Sternzeichen: Schütze
* Lieblingsfarbe: Pink

### Mag
* Laut sein und rumhüpfen ohne Grund
* Kampfsport; mit Fäusten und Tritten kämpfen
* Prinzessinenkleider (je pinker, rüschiger und auffälliger, desto besser)
* Märchen

### Mag nicht
* Ihre nervige Mutter
* Ihren nervigen Bruder
* Leute, die sie verbiegen wollen
* Ihre derzeitige Situation

### Besondere Eigenschaften
* Ist ein Halbherz und verwandelt sich in den wunderschönen Wasserdrachen Blauschuppe
* Sucht nach ihrem Märchenprinzen
* Spielt Blockflöte, würde aber lieber etwas lauteres und nervigeres spielen
* Trägt unter ihrer fröhlichen Fassade eine tiefe Trauer mit sich

## Trivia
* In der ursprünglichen Version sollte Lucias Fluch in nur einem Kapitel behandelt
  und aufgelöst werden
* In der Entwicklung der Story gab es viele potenzielle Märchenprinzen für sie,
  bis ich mich für einen entscheiden konnte
