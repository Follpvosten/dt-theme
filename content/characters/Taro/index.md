+++
title = "Taro"
description = "Der quirlige Ninjajunge aus Oker."
weight = 4
+++

***"Das war lustig!"***

Taro ist ein junger Ninja in Ausbildung, der in einem kleinen Dorf auf Oker
lebt.

## Steckbrief
* Alter: 16 Jahre
* Geburtstag: 29. Februar
* Sternzeichen: Fische
* Lieblingsfarbe: Orange
* Lieblingstier: Er kommt nicht gut mit Tieren klar...

### Mag
* Das Ninja-Leben
* Pizza mit extra viel Käse
* Käse generell
* Auf Bäume und Häuser hüpfen
* Monica, seine *beste Freundin!*

### Mag nicht
* Seinen Rivalen Kuzahse
* Seinen vollen Namen, Hotaru, da er sich so weiblich anhört
* Federvieh, Feuer, Vulkane, Geister und Menschen, die sich beschweren, wenn er
  auf ihr Dach springt
* Den Besitzer der Wüstenrenner-Farm

### Besondere Eigenschaften
* Hat eine sehr positiv eingestellte Persönlichkeit, die in so manchen dunklen
  Situationen erhellend sein kann
* Besitzt ein großes Herz und ist sehr hilfsbereit und aufopfernd, würde seine
  Freunde um jeden Preis beschützen
* Ist nicht gerade der schlauste und vermeidet Runen und Zahlen, soweit möglich
* Verliert sein Ziel niemals aus den Augen
* Kämpft bevorzugt mit Ninja-Waffen
* Kann trotz seines aufgedrehten Charakters in den richtigen Momenten auch ernst
  und überzeugend wirken

## Trivia
* Ursprünglich sollte Taro erst später vorkommen und auch erst später Monicas
  bester Freund werden.
* Taro war damals das Abbild meines imaginären besten Freundes, den ich immer
  haben wollte und auf diesen Charakter übertragen habe, weshalb ich ihn so
  liebe
