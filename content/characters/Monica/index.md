+++
title = "Monica"
description = "Die Protagonistin des Werkes."
weight = 1
+++

***Feuer beschützt. Feuer beruhigt. Feuer ist mein Freund.***

Monica ist die Protagonistin des Werkes.

## Steckbrief
* Alter: 15 Jahre
* Geburtstag: 15. August
* Sternzeichen: Löwe
* Lieblingsfarbe: Weinrot
* Lieblingstier: Vögel, besonders Adler

### Mag
* Zimtschnecken
* Geige spielen
* Kreuzworträtsel
* Bücher, besonders Horror, Thriller und Krimis
* Scharfes, asiatisches Essen
* Mathematik (besonders den Satz des Pythagoras)

### Mag nicht
* Eng anliegende Kleidung wie Hosen
* Stress und Fehlverhalten
* Leute, die zu sehr von sich überzeugt sind
* Hochgelegene Plätze, da sie Höhenangst hat

### Besondere Eigenschaften
* Obwohl ihr Zimmer unordentlich scheint, ist sie ein sehr organisierter Mensch
* Verbindet mit Feuer positive Emotionen
* Hat keinerlei künstlerische Begabung (ausgenommen die Geige, die sich ihrer erbarmt hat)
* Glänzt in der Schule in allen Fächern, in denen man aus Büchern lernen kann
* War als Kind ein großer Fan von Transformers und wollte immer ihren eigenen Autobot besitzen
* Hängt mit ihrem Kopf oft in den Wolken und hat manchmal wenig Ahnung davon,
  was um sie herum geschieht
* Hat eigentlich einen guten Sinn für Zwischenmenschliches - außer, es geht um
  sie selbst
* Kann absolut nicht kochen

## Trivia
* Die Namensgebung von Monica besitzt einen längeren Hintergrund und hat einige Entwicklungen durchgemacht:  
  Als Kind, und auch noch heute, bin ich ein großer Fan von RPG-Spielen gewesen und habe viele Titel durchgespielt.  
  Unter anderen gab es auch einen Titel, der es mir besonders angetan hatte: Dark Chronicle/Dark Cloud 2.  
  Die Hauptcharakterin trug den Namen Monica, den ich wunderschön fand und nun auch für meine Protagonistin,
  die damals noch keinen Namen hatte, verwendete. Doch mit der Zeit machte er einige Veränderungen durch.  
  So hieß Monica für kurze Zeit Moni**k**a, die deutsche Schreibweise. Da ich den Klang des "k" jedoch zu hart fand,
  wollte ich lieber die weichere Version zurück.  
  Zwischendurch änderte ich Monicas Namen auch zu Erika/Erica (inspiriert von Eirika, der Protagonistin von
  Fire Emblem: The Sacred Stones), doch fand nach einiger Zeit, dass dieser Name nicht wirklich zu ihr passte,
  und änderte ihn zurück. Bald fand ich meine alte Liebe zum Namen Monica und ihrer Namensgeberin wieder
  und fand so den nun endgültigen Namen der Heldin der Story: Monica.
