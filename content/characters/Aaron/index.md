+++
title = "Aaron"
description = "Monicas Geigenlehrer."
weight = 2
+++

***"Soll ich dir einen Tee machen?"***

Aaron ist Monicas Geigenlehrer und ein guter Freund der Familie.

## Steckbrief
* Alter: 58 Jahre
* Geburtstag: 25. März
* Sternzeichen: Widder
* Lieblingstier: Tauben

### Mag
* Tee
* Musik
* Romantik
* Graufeder, seine Haustaube

### Mag nicht
* Klatsch und Tratsch
* Sittenstrolche
* Seine Narben im Gesicht, für die er sich schämt
* Speisen mit Geflügel

### Besondere Eigenschaften
* Sieht für sein Alter erstaunlich gut aus
* Verschlossen und schweigsam
* Arbeitet als Musiklehrer in der ortsansässigen Grundschule
* Besitzt einen grünen Daumen und spielt viele Instrumente, aber kann
  überhaupt nicht kochen
* Ist ernst und gewissenhaft
* Wird von Frau Gurkenheimer angehimmelt

## Trivia
* Sollte ursprünglich im Verlauf der Story kaum anwesend sein
* Sein Name wird und wurde immer schon englisch ausgesprochen. Die Inspiration
  kam von einem Jungen aus meiner damaligen Schule, der ebenfalls so hieß.
  Ich fand den Namen schon damals schön.
