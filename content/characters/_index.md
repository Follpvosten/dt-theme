+++
title = "Charaktere"
description = "Die Charaktere, die zu Beginn des ersten Bandes am wichtigsten sind."
sort_by = "weight"
+++

Hier sind die wichtigsten Charaktere zu Beginn des ersten Bandes aufgelistet.
