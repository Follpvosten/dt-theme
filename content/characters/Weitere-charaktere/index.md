+++
title = "Weitere wichtige Charaktere"
description = "Kurze Infos über andere wichtige Charaktere."
weight = 5
+++

## Janine und Daan Vleugel
Monicas Adoptiveltern, die immer für ihre Tochter da sind.

## Frau Gurkenheimer
Die nette, alte Dame von nebenan, die gerne backt und immer den neusten Klatsch
und Tratsch verbreitet.

**Mini-Trivia**: *Frau Gurkenheimer ist von zwei realen Personen inspiriert, die
damals in meinem Dorf gelebt haben; einer stand ich sehr nah, von der anderen
habe ich nur gehört.*

## Henrietta
Monicas beste Freundin mit einem Faible für Piraten. Quelle für Monicas heimliche
Gelüste: Zimtschnecken.

**Mini-Trivia**: *Auf ihren vollen Namen "Henrietta Tina Finkel" bin ich komplett
zufällig gekommen und bin sehr stolz darauf.*

## Zuleika
Monicas Freundin, die ein Händchen für Mode besitzt und auch sonst künstlerisch
begabt ist. Sie ist von allen in Monicas Freundeskreis am meisten mit der
Popkultur vertraut, ist also ein Fan von Comics, RPG-Spielen und ähnlichem.

## Fiete
Der einzige Junge in Monicas Freundeskreis. Begeistert sich für Computer,
Roboter und das Reiten, besonders für sein Pferd Saturn. Entwickelt gerne
verrückte Verschwörungstheorien.

**Mini-Trivia**: *Ich wollte schon immer einen Charakter namens Fiete haben,
weil ich den Namen so süß fand. Mehr oder weniger inspiriert durch die Serie
"Die Pfefferkörner".*

## Dirk Autentahl
Der Leibwächter der Königsfamilie Monteno von Donnerhall. Steht Lucia am
nächsten und verwandelt sich in den Erddrachen Stachelstein.

## Seppuku
Ein mysteriöser Roboter-Samurai, der es sich zur Aufgabe gemacht hat, alle
Drachen zu vernichten.  
Doch was sind seine eigentlichen Absichten?  
Was will er damit erreichen?
