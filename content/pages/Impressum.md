+++
title = "Impressum"
description = "Impressum von DragonTale.de"
+++

### Private Anschrift der Autorin:

Aylin Hacker  
Am Hanlah 32  
31008 Elze  
Deutschland

### E-Mail:

[aylin.hacker@dragontale.de](mailto:aylin.hacker@dragontale.de)

### Telefon:

<a href="tel:+4369912488374">+43 699 / 124 883 74</a>
