+++
title = "Datenschutz"
description = "Datenschutzerklärung von DragonTale.de"
+++

Die Webseite "[DragonTale.de](https://dragontale.de)" verfügt über keinerlei
Mechanismen zur Aufzeichnung, Verarbeitung oder Weitergabe von Daten, weil uns
die Sicherheit und Privatsphäre unserer Benutzer sehr wichtig ist. Die Seite
verwendet keine Cookies, und es werden keine Access Logs geführt, durch die
Benutzer identifiziert werden könnten.

Das bedeutet, dass Ihre Aufrufe dieser Seite zu keinem Zeitpunkt für uns oder
Dritte nachvollziehbar sind; Sie können von dieser Webseite auf keine Weise
verfolgt oder identifiziert werden.

Ebenfalls verzichten wir vollständig auf die Einbindung jeglicher Inhalte von
Dritten, mit Ausnahme der TWENTYSIX-Shop-Widgets - die gesamte Seite ist auf
unseren Servern gehostet, sodass Sie hier auch nicht unfreiwillig von den großen
Datenkraken verfolgt werden können.

Kurz: Hier gibt es keine Kekse!
Bitte wenden Sie sich dafür an Frau Gurkenheimer.
