+++
title = "Einhörner"
description = "Und ihre verschiedenen Formen."
weight = 3
+++

Die verschiedenen Einhorn-Arten von Draconica sind sich zwar grundsätzlich im
Aussehen und Verhalten ähnlich, doch unterscheiden sich von Gebiet zu Gebiet
in verschiedenen Bereichen.

Es folgt eine grobe Übersicht der verschiedenen Einhorn-Arten.

## Grünewald
Diese Einhörner waren die ersten ihrer Art, da Einhörner ursprünglich aus
Grünewald stammen und sich über ganz Draconica verteilt und an ihre neuen
Lebensräume angepasst haben.

Sie besitzen schneeweißes Fell und eine geschwungene Mähne. Ihr Horn ist spitz
und ihre Beine sind lang und elegant; über ihren Hufen besitzen ein paar
ebenfalls geschwungene Fellbüschel.

## Flügelklippe
Diese besitzen ein sehr langes, dünnes Horn und große, spitze Ohren.  
Sie haben eine lockige, glitzernde Mähne, die teils rosane Strähnen aufweist.

Ein anderes besonderes Merkmal der Flügelklippe-Einhörner sind die Herzchen
auf ihren Wangen.

Sie sind im Vergleich zu den meisten anderen Einhorn-Arten eher selten
anzutreffen.

## Donnerhall
Donnerhall-Einhörner sind klein und dick, wie Bergziegen. Sie besitzen ein
ebenso kleines und rundes Horn, schlappe Ohren und eine zottelige, dichte
Mähne, die ihre Augen verdeckt.  
Außerdem besitzen sie dicke Beine und kräftige Hufe.

## Eisenwinde
Diese besitzen ein Horn mit rundlicher Spitze und kleine Ohren. Sie haben eine
wellige, lange Mähne und ein flauschiges, dickes Fell, welches sie im Schneesturm
warm hält.

## Blauküste
Die Blauküste-Einhörner sind eher von der gefährlichen Sorte und nicht so
harmlos, wie ihre andere Artgenossen.

Sie haben ein langes, dickes und spitzes Horn, kaum sichtbare Ohren, eine kurze
Mähne und eine lange Schnauze.

Sie unterscheiden sich äußerlich sehr stark von den anderen Einhörnern, da sie
zwar Vorderhufe, anstelle der Hinterhufe jedoch einen Fischschwanz besitzen.
