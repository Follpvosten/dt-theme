+++
title = "Andere besondere Wesen"
description = "Auswahl von anderen Lebenwesen, die in der Geschichte vorkommen."
weight = 5
+++

## Mischwesen
Obwohl sie etablierte Bürger Draconicas sind, existiert noch eine gewisse
Intoleranz ihnen gegenüber, zumindest bei manchen Bewohnern von Draconica.

Sie sehen aus wie Menschen, weisen jedoch gewisse Tiermerkmale auf, haben
beispielsweise Schweife oder Tierohren. Diese sind je nach Person verschieden
stark ausgeprägt; manche haben wirklich nur zusätzliche Körperteile, andere
sehen fast genau so aus, wie ihre tierischen Artgenossen, aber auf zwei Beinen.

Auch im Verhalten ähneln manche von ihnen ihren tierischen Verwandten.

## Wüstenrenner
Laufvögel, die Nandus ähneln und drei Köpfe besitzen. Sie werden meistens für
die Durchquerung der Wüste benutzt, da sie ausschließlich in Oker anzutreffen
sind, und können sich dank ihrer sandfarbenen Federn gut tarnen.  
Sie sind treue Reittiere, die sich nicht von der Stelle rühren, bis ihr Reiter
zurückgekehrt ist.

## Pflanzendämonen
Das sind Mensch-Blume-Hybriden mit riesigen, gelben Augen, die beinahe größer
als ihr Kopf sind. Sie sind etwa menschengroß.

Sie leben in Grünewald in einem Matriarchat und werden als höchst gefährlich
eingestuft. Sie sind den anderen Bewohnern gegenüber feindlich gesinnt.

## Lithowölfe
Dies sind Wölfe, aus kleinen Steinen zusammengesetzt, die sich hauptsächlich
in Bergen, Höhlen oder Gebirgen aufhalten.

Sie sind scheu und friedliebend. Konflikte bleiben meistens unter der Spezies.

## Schattenwölfe
Dunkle Wölfe mit roten Augen. Leben in dunklen Wäldern.

## Farbwölfe
Mischung aus Wölfen und verschiedenen Hunderassen, in allen vorstellbaren
Farben.

Sie besitzen lange, glitzernde, regenbogenfarbene Schweife, die mehrere Enden
haben und selbst bei absoluter Windstille zu wehen scheinen.

Ihre Grundfarbe zeigt ihre Rangordnung an; die purpurnen Farbwölfe stehen über
den anderen.

## Vulpura
Lilane Füchse mit vier Ohren und drei Schweifen. Sie sind ungefähr so groß,
wie ein Corgi, und haben flauschiges Halsfell.

Sie sind eine Kreuzung aus Kitsune und Farbwölfen.

Während sie hübsch anzusehen sind, verfügen sie über keine anderen besonderen
Eigenschaften.

## Virix
Das sind grüne Eulen mit zwei Köpfen.

Sie sind Besserwisser, nervig und plappern gerne viel. Sie lassen keine
Gelegenheit aus, um mit ihrem Wissen anzugeben.
