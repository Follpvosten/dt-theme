+++
title = "Die Puis"
description = "Kleine, flauschige Wesen, die in ganz Draconica zu finden sind."
weight = 2
+++

*Pui* ist ein grober Überbegriff für eine Vielzahl von Rassen, die sich
teilweise grundsätzlich voneinander unterscheiden. Die meisten von ihnen
ähneln verschiedenen Nagetieren.

## Puininchen
Diese Art ähnelt den Hasen. Sie sind im Durchschnitt kniehoch (abzüglich
ihrer Möhrchen-Öhrchen - so nennt man bei den Puininchen die Löffel) und
sind als Magier aktiv.  
Je flauschiger ein Puininchen ist, desto mehr magische Kraft besitzt es.

Diese Puis sind von allen am häufigsten anzutreffen und in Draconica weit
verbreitet. Ihr ursprünglicher Lebensraum war jedoch, wie bei vielen
Fabelwesen, Grünewald.

## Flederpuis
Diese auf Fledermäusen basierenden Puis sind wesentlich kleiner, als die
Puininchen; sie messen im Durchschnitt etwa einen Fuß (30cm).

Sie leben bevorzugt im Schatten der Bäume, tragen Mäntel mit Kapuzen, um
sich bedeckt zu halten, und sind talentierte Bogenschützen.

Sie sind hauptsächlich in Sturm anzutreffen.

## Knusperpuis
Die Knusperpuis ähneln ihren irdischen Mausverwandten in Größe und Aussehen.  
Sie agieren oft unbemerkt und werden als Spitzel und Spione eingesetzt.  
Ihr extrem flauschiges Fell lädt sich schnell statisch auf, sodass sie bei
Berührung ungewollte Stromstöße verteilen.

Ihr Hauptlebensraum ist Grünewald.

## Nusspui
Diese Puis ähneln den Eichhörnchen, mit dem Unterschied, dass sie auf zwei
Beinen stehen, so groß wie die Puininchen sind und Rüstungen, Schwerter
und Schilde tragen.

Die Nusspuis sind die Wächter von Grünewald und starke, heroische Krieger,
die man nicht unterschätzen sollte.

Ihr Stamm befindet sich in Grünewald.

## Puimuhchen
Diese Art Puis unterscheidet sich völlig von den anderen. Sie sind kleine Kühe
auf zwei Beinen und sind etwa so groß wie Puininchen.

Sie tragen kleine Kleidchen und eine Glocke um den Hals, deren Klang beruhigend
wirkt. Auf ihren Hörnern finden sich verschiedene Muster wieder.

Sie agieren häufig als Heiler und arbeiten auf der Puimuhchen-Farm in der
Flügelklippe. Ihre Milch zeugt seit hunderten von Jahren von Qualität.

## Pui unter der Decke
Über diese Puis ist wenig bekannt.  
Sie verstecken sich unter einer Decke, daher weiß niemand, wie sie aussehen.
Manchmal schnappen sie sich ahnungslose Passsanten, ziehen sie unter ihre Decke
und knuddeln sie.

## Eck-Steck Puis
Sind ebenso unbekannt und verstecken sich öfter hinter Ecken, wo man nur ihre Ohren
hervorblitzen sieht. Niemand hat sie jemals zu Gesicht bekommen.
