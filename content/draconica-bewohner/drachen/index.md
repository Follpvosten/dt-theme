+++
title = "Drachen"
description = "Die hauptsächlichen Bewohner Draconicas."
weight = 1
+++

Drachen werden grundsätzlich in drei Unterkategorien unterteilt:

* Drachen-Nachfahren: Dies sind die auf den ersten Blick normal scheinende
  Menschen, die jedoch magische Fähigkeiten besitzen können und von Drachen
  abstammen.
* Halbherzen (oder auch Drachen): Genau wie die Nachfahren sehen die Halbherzen
  normalerweise aus, wie normale Menschen. Sie besitzen jedoch eine Drachenform,
  in die sie sich nach belieben verwandeln können.
* Vollblut-Drachen (oder auch Vollblüter): Als Gegenstück zum Nachfahren können
  sich diese Drachen nicht in eine menschliche Form verwandeln, sondern besitzen
  nur eine Drachenform.

