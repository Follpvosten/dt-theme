+++
title = "Bewohner Draconicas"
description = "Informationen über verschiedene Lebewesen, die in Draconica leben."
sort_by = "weight"
+++

In Draconica sind neben den Drachen auch vielerlei andere magische Wesen beheimatet,
wie beispielsweise Meerjungfrauen, Greifen und Zentauren.

Es finden sich auch einige exklusive Geschöpfe wieder, die man nur aus
Draconica kennen wird.

Hier findet sich eine grobe Übersicht der besonders interessanten und vielfach
vertretenen Spezies, gefolgt von einer kurzen Liste der weiteren Bewohner
mitsamt Lebensraum.
