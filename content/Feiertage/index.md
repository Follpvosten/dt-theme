+++
title = "Feiertage"
description = "Infos über die Feiertage von Draconica."
+++

In den verschiedenen Gebieten von Draconica gibt es jeweils lokale Feiertage,
sowie auch Draonica-nationale Feiertage. Es folgt eine Liste aller Feiertage
von Draconica.

* Januar
  * 1.: Lavanacht/Nationalfeiertag Flügelklippe
  * 12.: Tag der heiligen Lichtschweif
  * 26.: Nationalfeiertag Donnerhall
* Februar
  * 14.: Der Flammendes Herz-Tag
  * 21-22.: TaFira - Tag des Feuers
  * Variierender Tag: Narrenwettbewerb in Flügelklippe
* März
  * 24.-25.: Draconicanischer Tag der Natur
* April
  * Erster Sonntag im Monat: Tag des Puis
* Mai
  * 21.: Tag des Drachen
* Juni
  * Zweiter Samstag im Monat: Internationaler Knuspertag
* Juli
  * 2.: Draconicanischer Tag des Wassers
  * 3.: Nationalfeiertag Sturm
  * 15.: Nationalfeiertag Blauküste
* August
  * Erste Woche: B'quana. Das wichtigste Fest in Draconica.
* September
  * 6.: Nationalfeiertag Oker
  * 17.: Nationalfeiertag Eisenwinde
  * 28.: Draconicanischer Tag des Drachenwein
* Oktober
  * 3.: Nationalfeiertag Grünewald
  * 4.: Heiliger Draconicanus
* November
  * 13.: Internationaler Tag des Muh-Gütesiegels
  * 20.: Redemianischer Tag des Glanzlichtes
* Dezember
  * 6.: Tag des Eiszapfen
  * 23.: Nationalfeiertag Redemia
* Beliebiger Tag im Jahr: Sturmfest
